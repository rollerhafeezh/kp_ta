<div class="card mb-0">
	<div class="card-header pb-0">
		<h6><?= $title ?></h6>
	</div>
	<div class="card-body">
		<?php if (count($penguji) < 1): ?>
			<div class="font-small-3 alert alert-warning text-center">Dosen Penguji belum di-ploting oleh administrator. <br>
				Silahkan hubungi <b>Bagian Akademik</b> atau <b>Ketua Program Studi</b>.</div>
		<?php else: ?>
		<div class="row">
			<!-- Seminar / Sidang Online -->
			<?php
			$jadwal_sekarang = json_decode($this->curl->simple_get(ADD_API.'aktivitas/penjadwalan?id_aktivitas='.$aktivitas_mahasiswa[0]->id_aktivitas.'&tanggal='.date('Y-m-d').'&mulai='.date('H:i:s').'&selesai='.date('H:i:s', strtotime('+15 minutes')))) ?: [];

			if (count($jadwal_sekarang) > 0) {
			?>
			<div class="col-md-12">
				<div id="meet"></div>
				<script src='https://meet.jit.si/external_api.js'></script>
				<script type="text/javascript">
				const domain = 'meet.jit.si';
				const options = {
				    roomName: '<?= $jadwal_sekarang[0]->event_id ?>',
				    width: '100%',
				    height: 500,
				    parentNode: document.querySelector('#meet'),
				    interfaceConfigOverwrite: {
				    	TOOLBAR_BUTTONS: [
					        'microphone', 'camera', 'closedcaptions', 'desktop', 'fullscreen',
					        'fodeviceselection', 'hangup', 'profile', 'chat', 'recording',
					        'livestreaming', 'etherpad', 'settings', 'raisehand',
					        'videoquality', 'filmstrip', 'invite', 'feedback', 'stats', 'shortcuts',
					        'tileview', 'videobackgroundblur', 'download', 'help', 'mute-everyone',
					        'e2ee', 'security'
					    ]
				    },
				    userInfo: {
				        displayName: '<?=$_SESSION['nama_user']?>'
				    }
				};
				const api = new JitsiMeetExternalAPI(domain, options);
			 	api.executeCommand('subject', '<?= $jadwal_sekarang[0]->nama_kegiatan.' ('.$aktivitas_mahasiswa[0]->id_aktivitas.')' ?>');
				</script>
				<div class="clearfix d-block m-1"></div>
			</div>
			<?php } ?>
			<!-- Seminar / Sidang Online -->

			<div class="col-md-5">
				<!-- Informasi Dosen Penguji -->
				<fieldset style="border: 1px solid #BABFC7; margin: inherit;" class="px-1 pt-1 unggah-berkas">
					<legend style="width: inherit; font-size: inherit; margin: inherit;" class="font-small-3 pl-1 pr-1">
						<b>Informasi Dosen Penguji</b>
					</legend>

					<?php $no = 1; foreach ($penguji as $r_penguji):  ?>
					<table border="0" cellspacing="0" cellpadding="3" class="font-small-3 mb-1">
						<tr>
							<td colspan="3"><b>Penguji Ke-<?= $r_penguji->penguji_ke ?></b></td>
						</tr>
						<tr>
							<td width="100" valign="top">Nama Dosen</td>
							<td valign="top">:</td>
							<td><?= $r_penguji->nm_sdm ?></td>
						</tr>
						<tr>
							<td>NIDN</td>
							<td>:</td>
							<td><?= $r_penguji->nidn ?></td>
						</tr>
						<tr>
							<td>No. HP</td>
							<td>:</td>
							<td><?= preg_replace('/^62/', '0', $r_penguji->no_hp) ?></td>
						</tr>
					</table>
					<?php endforeach; ?>
				</fieldset>
				<!-- Informasi Dosen Penguji -->

				<div class="clearfix d-block m-1"></div>

				<!-- Informasi Aktivitas -->
				<fieldset style="border: 1px solid #BABFC7; margin: inherit;" class="p-1 unggah-berkas">
					<legend style="width: inherit; font-size: inherit; margin: inherit;" class="font-small-3 pl-1 pr-1">
						<b>Informasi Aktivitas</b>
					</legend>

					<table border="0" cellspacing="0" cellpadding="3" class="font-small-3">
						<tr>
							<td width="100" valign="top">Jenis Aktivitas</td>
							<td valign="top">:</td>
							<td><?= ucwords(strtolower($usulan[0]->nm_mk)) ?></td>
						</tr>
						<tr>
							<td>T. Akademik</td>
							<td>:</td>
							<td><?= $usulan[0]->nama_semester ?></td>
						</tr>
						<tr>
							<td>Program Studi</td>
							<td>:</td>
							<td><?= $detail->nama_prodi ?></td>
						</tr>
						<tr>
							<td>Lokasi</td>
							<td>:</td>
							<td><?= $aktivitas_mahasiswa[0]->lokasi ?></td>
						</tr>
						<tr>
							<td valign="top">Judul</td>
							<td valign="top">:</td>
							<td><?= $aktivitas_mahasiswa[0]->judul ?></td>
						</tr>
						<tr>
							<td valign="top">Jenis Anggota</td>
							<td valign="top">:</td>
							<td><?= $aktivitas_mahasiswa[0]->jenis_anggota == '0' ? 'Personal' : 'Kelompok' ?></td>
						</tr>
					</table>
				</fieldset>
				<!-- Informasi Aktivitas -->
				
				<div class="clearfix d-block m-1"></div>

				<!-- Informasi Peserta -->
				<fieldset style="border: 1px solid #BABFC7; margin: inherit;" class="p-1 unggah-berkas">
					<legend style="width: inherit; font-size: inherit; margin: inherit;" class="font-small-3 pl-1 pr-1">
						<b>Informasi Peserta (<?= count($anggota) ?> Orang)</b>
					</legend>
					<style type="text/css">
						.table td,.table  th {
							padding: 10px !important;
						}
					</style>
					<table border="0" cellspacing="0" class="w-100 table-sm table-hover font-small-3">
						<thead>
							<tr>
								<!-- <th>No.</th> -->
								<th>NPM</th>
								<th>Nama Mahasiswa</th>
								<th>Peran</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; $jenis_peran = ['-', 'Ketua', 'Anggota', 'Personal']; foreach ($anggota as $r_anggota): ?>
							<tr>
								<!-- <td><?= $no; $no++ ?>.</td> -->
								<td><?= $r_anggota->id_mahasiswa_pt ?></td>
								<td><?= $r_anggota->nm_pd ?></td>
								<td><?= $jenis_peran[$r_anggota->jenis_peran] ?></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</fieldset>
				<!-- Informasi Peserta -->
			</div>

			<div class="w-100 d-block d-md-none m-1"></div>

			<div class="col-md-7">
				<!-- <div class="row mt-1">
					<div class="col-md-12">
						<textarea class="form-control font-small-3" placeholder="Tulis Materi ..." rows="5"></textarea>
					</div>
					<div class="w-100 d-block mt-1"></div>
					<div class="col-8 font-small-3" style="vertical-align: middle; line-height: 2.4">
						<a href="#" data-toggle="tooltip" title="Klik untuk melampirkan dokumen."><i class="ft-paperclip"></i> Lampirkan Dokumen</a>
					</div>
					<div class="col-4">
						<button class="btn btn-success w-100 btn-sm"><i class="ft-navigation"></i> KIRIM</button>
					</div>
				</div>
				<hr> -->
				<fieldset style="border: 1px solid #BABFC7; margin: inherit;" class="px-1 unggah-berkas">
					<legend style="width: inherit; font-size: inherit; margin: inherit;" class="font-small-3 pl-1 pr-1">
						<b>Catatan Revisi</b>
						&nbsp; <a target="_blank" href="<?= base_url('bimbingan/logbook_penguji') ?>" class="badge badge-info"><i class="ft-download"></i> Unduh</a>
					</legend>
					<div class="aktivitas overflow-auto px-1" style="height: 95%"></div>
				</fieldset>
			</div>
		</div>
	<?php endif; ?>
	</div>
</div>
<script>
	var is_open = false
	var source = new EventSource('/bimbingan/sse')
	var offline

	source.onmessage = function(event) {
		if (!$('input').is(':focus')) {
			if (areAllInputsEmpty() && $('input:hover').length == 0 && is_open == false) {
				aktivitas()
				// console.log('aktivitas()')
			}
		}

		// console.log(is_open, $('input:hover').length, $('input').is(':focus'), areAllInputsEmpty())
	}

	$('input').click(function() {
		is_open = true
	})

	function areAllInputsEmpty() {
	  return $("input").filter(function() {
	    return $.trim($(this).val()).length > 0
	  }).length == 0
	}

	function aktivitas()
	{
		fetch('/bimbingan/aktivitas/2')
		.then(response => response.text())
		.then(text => {
			document.querySelector('.aktivitas').innerHTML = text
		})
		.then( () => {
			timeago.render(document.querySelectorAll(".timeago"), "id_ID")
			$('.tooltip').hide()
			// $('[data-toggle="tooltip"]').tooltip()
			$("body").tooltip({ selector: '[data-toggle=tooltip]' })
			is_open = false
			upload = null
		})
	}

	var upload;
	function lampirkan_dokumen(e) {
		e.parentElement.children[1].innerHTML = `<span class="text-nowrap overflow-hidden" style="width: 200px; text-overflow: ellipsis;">${e.files[0].name}</span> - <a onclick="reset(this, event)" class="text-danger">hapus</a>`
		upload = e
		// console.log(upload)
	}

	function reset(e, event) {
		event.preventDefault()
		var inputFile = e.parentElement.parentElement.children[0]
		var label = e.parentElement

		$(inputFile).val('')
		label.innerHTML = '<i class="ft-paperclip"></i>  Lampirkan Berkas'
		is_open = false
	}

	function kirim(e, event) {
		if (e.value != '' && event.keyCode == '13') {
			e.setAttribute('disabled', 'true')
			var formData = new FormData()
			formData.append('isi', e.value)
			formData.append('id_parent', e.dataset.id_parent)
			formData.append('id_aktivitas', e.dataset.id_aktivitas)
			formData.append('jenis_bimbingan', '2')
			
			if (upload)
				formData.append('file', upload.files[0])

			fetch('/bimbingan/kirim', {
				method: 'POST',
				body: formData
			})
			.then(response => response.text())
			.then(text => {
				aktivitas()
				is_open = false
			})
		}
	}

	function hapus(e) {
		var konfirmasi = confirm('Bade Dihapus ?')
		if (konfirmasi) {
			$('input').attr('disabled', 'true')
			fetch('/bimbingan/hapus', {
				method: 'POST',
				body: new URLSearchParams({ 
						id_bimbingan: e.dataset.id_bimbingan, 
						file: e.dataset.file
					})
			})
			.then(response => response.text())
			.then(text => {
				aktivitas()
				is_open = false
			})
		}
	}
</script>