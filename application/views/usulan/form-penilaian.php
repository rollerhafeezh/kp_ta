<!DOCTYPE html>
<html>
<head>
	<title><?= $title ?></title>
	<style type="text/css">
		@page {
			margin-top: 10mm;
			margin-header: 0;
		}
	</style>
</head>
<body style="font-family: calibri; font-size: 11pt">
	<table width="100%">
		<tr>
			<td>
				<center>
					<h3>FORM PENILAIAN SEMINAR <?= strtoupper($usulan[0]->nm_mk) ?> (<?= acronym($usulan[0]->nm_mk) ?>)</h3>
				</center>
	    	</td>
		</tr>
	</table>
	<br>
	<table border="0" width="100%" cellpadding="1" style="margin-left: 40px;">
		<tr>
			<td width="200">Nama Mahasiswa</td>
			<td width="15">:</td>
			<td><?= $detail->nm_pd ?></td>
		</tr>
		<tr>
			<td>Nomor Pokok Mahasiswa</td>
			<td>:</td>
			<td><?= $detail->id_mahasiswa_pt ?></td>
		</tr>
		<tr>
			<td>Lokasi</td>
			<td>:</td>
			<td><?= $aktivitas_mahasiswa[0]->lokasi ?></td>
		</tr>
		<tr>
			<td valign="top">Judul <?= ucwords(strtolower($usulan[0]->nm_mk)) ?></td>
			<td valign="top">:</td>
			<td style="line-height: 1.5"><?= count($aktivitas_mahasiswa) > 0 ? strip_tags($aktivitas_mahasiswa[0]->judul) : '-' ?></td>
		</tr>
		<tr>
			<td>Pembimbing</td>
			<td>:</td>
			<td><?= $pembimbing[0]->nm_sdm ?></td>
		</tr>
	</table>
	<br>
	<table border="1" width="100%" cellpadding="5" cellspacing="0">
	  <tr>
	  	<th>Komponen</th>
	  	<th>Nilai Pembimbing</th>
	  	<th>Nilai Penguji</th>
	  </tr>
	  <tr>
	  	<td height="40">
	  		1. Nilai Proses Bimbingan
	  	</td>
	  	<td align="center"><?= count($nilai_pembimbing) < 1 ? '-' : $nilai_pembimbing[0]->nilai_1 ?></td>
	  	<td align="center" bgcolor="silver"></td>
	  </tr>
	  <tr>
	  	<td height="40">
	  		2. Nilai Laporan
	  	</td>
	  	<td align="center"><?= count($nilai_pembimbing) < 1 ? '-' : $nilai_pembimbing[0]->nilai_2 ?></td>
	  	<td align="center"><?= count($nilai_penguji) < 1 ? '-' : $nilai_penguji[0]->nilai_2 ?></td>
	  </tr>
	  <tr>
	  	<td height="40">
	  		3. Nilai Presentasi / Jawaban Pertanyaan
	  	</td>
	  	<td align="center"><?= count($nilai_pembimbing) < 1 ? '-' : $nilai_pembimbing[0]->nilai_3 ?></td>
	  	<td align="center"><?= count($nilai_penguji) < 1 ? '-' : $nilai_penguji[0]->nilai_3 ?></td>
	  </tr>
	  <tr>
	  	<td valign="middle" align="center" height="40">RATA-RATA</td>
	  	<td align="center"><?= count($nilai_pembimbing) < 1 ? '-' : ceil($nilai_pembimbing[0]->rata_rata_nilai) ?></td>
	  	<td align="center"><?= count($nilai_penguji) < 1 ? '-' : ceil($nilai_penguji[0]->rata_rata_nilai) ?></td>
	  </tr>
	</table>
	<br>
	<table width="100%" border="0" cellpadding="5">
		<tr>
			<td colspan="5">Perincian nilai selengkapnya adalah: <br> &nbsp;</td>
		</tr>
		<tr>
			<td width="200">
				1. &nbsp;&nbsp;Nilai Pembimbing
			</td>
			<td width="40" style="border-bottom:  1px solid black;" align="center">
				<?= count($nilai_pembimbing) < 1 ? '-' : ceil($nilai_pembimbing[0]->rata_rata_nilai) ?>
			</td>
			<td width="60">* 0.7 =</td>
			<td width="40" style="border-bottom:  1px solid black;" align="center">
				<?= $nilai_pemb = count($nilai_pembimbing) < 1 ? '-' : ceil($nilai_pembimbing[0]->rata_rata_nilai * 0.7) ?>
			</td>
			<td></td>
		</tr>
		<tr>
			<td width="200">
				2. &nbsp;&nbsp;Nilai Penguji
			</td>
			<td width="40" style="border-bottom:  1px solid black;" align="center">
				<?= count($nilai_penguji) < 1 ? '-' : ceil($nilai_penguji[0]->rata_rata_nilai) ?>
			</td>
			<td width="60">* 0.3 =</td>
			<td width="40" style="border-bottom:  1px solid black;" align="center">
				<?= $nilai_peng = count($nilai_penguji) < 1 ? '-' : ceil($nilai_penguji[0]->rata_rata_nilai * 0.3) ?>
			</td>
			<td></td>
		</tr>
		<tr>
			<td style="white-space: nowrap; line-height: 1.5">
				Sehingga nilai akhir yang dicapai adalah: <u>&nbsp;&nbsp;&nbsp;&nbsp; <?= $nilai_pemb + $nilai_peng ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
			</td>
		</tr>
		<tr>
			<td style="white-space: nowrap; line-height: 1.5">
				Dengan huruf mutu: <u>&nbsp;&nbsp;&nbsp;&nbsp; <?= nilai_mutu($nilai_pemb + $nilai_peng) ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
			</td>
		</tr>
		<tr>
			<td colspan="5" style="line-height: 1.5">
				Mahasiswa diwajibkan mengumpulkan laporan kerja praktek (sudah dijilid <i>hardcover</i>) selambat-lambatnya tanggal <u>&nbsp;&nbsp;&nbsp;<?= date_indo(date("Y-m-d", strtotime("+7 day", strtotime($penjadwalan->tanggal)))) ?>&nbsp;&nbsp;&nbsp;</u>
			</td>
		</tr>

	</table>
	<br>
	<br>
	<table width="100%">
		<tr>
			<td align="center" width="40%">
				<br>
				<?php if(count($penguji) > 0): ?>
				Dosen Penguji,
	     	 	<br>
				<br>
				<?= $data == 1 ? 'TTD' : '<br>' ?>
				<br>
				<br>
				<strong style="text-decoration: underline;"><?= $penguji[0]->nm_sdm ?></strong>
				<?php endif; ?>
			</td>
	    <td width="20%"></td>
			<td align="center" width="40%">
				Majalengka, <?= date_indo(date('Y-m-d')) ?> <br>
				Dosen Pembimbing,
				<br>
				<br>
				<?= $data == 1 ? 'TTD' : '<br>' ?>
				<br>
				<br>
				<strong style="text-decoration: underline;"><?= $pembimbing[0]->nm_sdm ?></strong>
			</td>
		</tr>
	</table>
	<br>
	<table>
		<tr>
			<td>Ketentuan huruf mutu:</td>
		</tr>
		<tr>
			<td>A (Baik Sekali)</td>
			<td>(80 &#8804; NA &#8804; 100)</td>
		</tr>
		<tr>
			<td>B (Baik)</td>
			<td>(70 &#8804; NA &#8804; 79)</td>
		</tr>
		<tr>
			<td>C (Cukup)</td>
			<td>(50 &#8804; NA &#8804; 69)</td>
		</tr>
		<tr>
			<td>D (Kurang)</td>
			<td>(NA &#8804; 49) harus mengulang seminar/sidang</td>
		</tr>
	</table>
</body>
</html>