<div class="card">
	<div class="card-header pb-1">
		<h6><?= $title ?> <?= ucwords(strtolower($usulan[0]->nm_mk)) ?></h6>

		<div class="heading-elements">
			<button class="btn btn-sm btn-info" data-toggle="modal" data-target="#modal-jadwal"><i class="ft-calendar"></i> Jadwal Seminar / Sidang</button>

			<div class="modal fade text-left" id="modal-jadwal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel17" bis_skin_checked="1" aria-hidden="true" style="display: none;">
				<div class="modal-dialog modal-lg" role="document" bis_skin_checked="1">
					<div class="modal-content" bis_skin_checked="1">
						<div class="modal-header border-bottom-0" bis_skin_checked="1">
							<h6 class="modal-title" id="myModalLabel17">Jadwal Seminar / Sidang</h6>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span>
							</button>
						</div>
						<div class="modal-body p-0 rounded-bottom" bis_skin_checked="1">
							<style type="text/css">
								.table th, .table td {
									padding: 10.5px !important;
								}

								.upload { 
									filter: grayscale(100%);
								}

								table.jadwal-seminar-sidang tr:last-child td {
								    border: none;
								}
							</style>
							<table class="table table- mb-0 font-small-3 table-hovered jadwal-seminar-sidang">
								<thead>
									<tr style="background: #eaf3fc;">
										<th  width="1">No</th>
										<th>Nama Kegiatan</th>
										<th>Dosen Penguji</th>
										<th>Tempat</th>
										<th>Tanggal</th>
										<th width="1">Mulai</th>
										<th width="1">Selesai</th>
										<!-- <th>Status</th> -->
									</tr>
								</thead>
								<tbody>
									<?php if (count($penjadwalan) < 1): ?>
									<tr>
										<td class="text-italic text-center" colspan="7">Jadwal Kegiatan Masih Kosong.</td>
									</tr>
									<?php else:
									$no = 1; 
									foreach ($penjadwalan as $r_penjadwalan) { ?>
									<tr>
										<td><?= $no; $no++ ?></td>
										<td><?= $r_penjadwalan->nama_kegiatan; ?></td>
										<td>
											<?php
												$list_penguji = json_decode($this->curl->simple_get(ADD_API.'aktivitas/penguji?id_aktivitas='.$r_penjadwalan->id_aktivitas.'&id_kegiatan='.$r_penjadwalan->id_kegiatan)) ?: [];
												foreach ($list_penguji as $r_penguji) {
													echo "<span data-toggle='tooltip' title='Penguji ke-$r_penguji->penguji_ke'>$r_penguji->nm_sdm</span>,<br>";
												}
											?>
										</td>
										<td><a target="_blank" href="<?= $r_penjadwalan->tautan ?>"><?= $r_penjadwalan->tempat; ?></a></td>
										<td><a target="_blank" href="<?= $r_penjadwalan->event_link ?>" target="_blank"><?= konversi_hari(date('w', strtotime($r_penjadwalan->tanggal))).', '.date_indo($r_penjadwalan->tanggal) ?></a></td>
										<td><?= $r_penjadwalan->mulai; ?></td>
										<td><?= $r_penjadwalan->selesai; ?></td>
									</tr>
									<?php }
									endif; ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="card-content table-responsive">
		<style type="text/css">
			.table th, .table td {
				padding: 10.5px !important;
			}

			.upload { 
				filter: grayscale(100%);
			}
		</style>
		<table class="table table- mb-0 font-small-3 table-hovered">
			<thead>
				<tr style="background: #eaf3fc;">
					<th  width="1">No</th>
					<th>Identitas Pengusul</th>
					<th>Lokasi</th>
					<th >Usulan</th>
					<!-- <th>Reg</th> -->
					<!-- <th>Cetak</th> -->
					<th>Status</th>
					<th>Nilai</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="text-center">1.</td>
					<td>
						<b class="d-block"><?= $detail->nm_pd ?></b>
						<span><?= $detail->id_mahasiswa_pt ?></span> <br>
						<i><?= $detail->nama_prodi ?></i>
					</td>
					<td>
						<?php if (count($aktivitas_mahasiswa) > 0): ?>
						<?= $aktivitas_mahasiswa[0]->lokasi != '' ? $aktivitas_mahasiswa[0]->lokasi.' - ' : '' ?> 
						<a href="<?= base_url('usulan/atur_lokasi') ?>" class="font-small-3 text-nowrap">Atur Lokasi</a>
						<?php else: ?>
						<a href="#" data-toggle="tooltip" title="Anda belum didaftarkan pada kegiatan <?= $usulan[0]->nm_mk ?>, Silahkan hubungi bagian akademik." class="font-small-3 text-nowrap text-muted" style="cursor: not-allowed;">Atur Lokasi</a>
						<?php endif; ?>
					</td>
					<td>
						<span class="status-usulan d-block pb-1">
							<?= $usulan[0]->nama_semester ?> : <?= (count($usulan) < 2) ? 'Usulan Baru' : 'Usulan Lanjut <img src="http://icons.iconarchive.com/icons/danrabbit/elementary/16/Button-info-icon.png" data-toggle="tooltip" title="Dari Tahun Akademik '.$usulan[count($usulan)-1]->nama_semester.'">' ?>
							
						</span>

						<?php if (count($aktivitas_mahasiswa) > 0): ?>
							<?php if ($aktivitas_mahasiswa[0]->judul != ''): ?>
								<span class="judul-usulan font-weight-bold">
									<?= strip_tags($aktivitas_mahasiswa[0]->judul) ?>
								</span> - <a href="<?= base_url('usulan/masukkan_judul') ?>" class="font-small-3">Ubah Judul</a>
							<?php else: ?>
								<a href="<?= base_url('usulan/masukkan_judul') ?>" class="font-small-3">Masukkan Judul <?= ucwords(strtolower($usulan[0]->nm_mk)) ?></a>
							<?php endif; ?>
						<?php else: ?>
							<a href="#" data-toggle="tooltip" title="Anda belum didaftarkan pada kegiatan <?= $usulan[0]->nm_mk ?>, Silahkan hubungi bagian akademik." class="font-small-3 text-muted" style="cursor: not-allowed;">Judul <?= $usulan[0]->nm_mk ?></a>
						<?php endif; ?>
						

						<span class="d-block pt-1">
							<b>Pembimbing</b> : 
							<?php
							$pmb_text = '';
							if (count($pembimbing) > 0)  {
								foreach ($pembimbing as $pmb) {
									$pmb_text .= '<span data-toggle="tooltip" title="Pembimbing ke '.$pmb->pembimbing_ke.'"><sup>['.$pmb->pembimbing_ke.']</sup>'.ucwords(strtolower($pmb->nm_sdm)).'</span>, ';
								}
								echo rtrim($pmb_text, ', ');
							} else { echo '-'; }
							?>
							<br>
							<b>Penguji/Penelaah</b> : 
							<?php
							$penguji_ = '';
							if (count($penguji) > 0)  {
								foreach ($penguji as $r_penguji) {
									$penguji_ .= '<span data-toggle="tooltip" title="Penguji ke '.$r_penguji->penguji_ke.'"><sup>['.$r_penguji->penguji_ke.']</sup>'.ucwords(strtolower($r_penguji->nm_sdm)).'</span>, ';
								}
								echo rtrim($penguji_, ', ');
							} else { echo '-'; }
							?>
						</span>
						
						<ul class="p-0 mt-1 list-unstyled font-small-3 text-nowrap">
							<li><a href="<?= base_url('usulan/pendaftaran') ?>" target="_blank"><img src="http://a0.pise.pw/QJGVA"> Download Formulir Pendaftaran</a></li>
							<li><a href="<?= base_url('usulan/pendaftaran_seminar') ?>" target="_blank"><img src="http://a0.pise.pw/QJGVA"> Download Formulir Pendaftaran Seminar</a></li>
							<li><a href="<?= base_url('usulan/laporan_kemajuan') ?>" target="_blank"><img src="http://a0.pise.pw/QJGVA">  Download Laporan Kemajuan / Jurnal</a></li>
							<?php if (count($pembimbing) > 0 && count($penguji) > 0 && count($penjadwalan) > 0): ?>
							<li><a href="<?= base_url('usulan/berita_acara') ?>" target="_blank"><img src="http://a0.pise.pw/QJGVA">  Download Berita Acara Seminar KP / PKL</a></li>
							<?php endif; ?>
							<li><a href="<?= base_url('usulan/bukti_penyerahan_laporan') ?>" target="_blank"><img src="http://a0.pise.pw/QJGVA">  Download Bukti Penyerahan Laporan (Perpustakaan)</a></li>
							<!-- <li><a href="#">Download Pedoman Tugas Akhir</a></li> -->
						</ul>
					</td>
					<!-- <td class="align-middle">
						<i class="ft-check-circle text-success"></i>
					</td> -->
					<!-- <td>
						<a href="#">Daftar Hadir</a>
					</td> -->
					<td class="font-small-3">
						<b class="mb-1 badge badge-<?= $aktivitas_mahasiswa[0]->status == "0" ? "danger" : "success" ?> font-small-3"><?= $aktivitas_mahasiswa[0]->status == "0" ? "Belum Lengkap" : "Lengkap" ?></b>
						
						<ul class="p-0 m-0 list-unstyled font-small-3 text-nowrap ">
							<?php
								$berkas = json_decode($this->curl->simple_get(ADD_API.'aktivitas/berkas?id_jenis_aktivitas_mahasiswa=6&jenis_aktivitas=kp&id_mahasiswa_pt='.$detail->id_mahasiswa_pt));
								// $berkas = json_decode($this->curl->simple_get(ADD_API.'aktivitas/berkas?jenis_usulan=kp&id_mahasiswa_pt='.$detail->id_mahasiswa_pt));
								foreach ($berkas as $row) {
								?>
									<li>
										<a href="<?= $row->berkas != '' ? base_url('berkas/kp/'.$row->berkas).'" target="_blank"' : base_url('usulan/upload/'.$row->id_kat_berkas) ?>" data-toggle="tooltip" title="Tgl. Upload: <?= $row->timestamp != '' ? date_indo(explode(' ', $row->timestamp)[0]) : '-' ?>">
												<img 
													class="<?= $row->berkas == '' ? 'upload' : '' ?>"
													src="http://a0.pise.pw/QJGVA"> <?= $row->berkas == '' ? 'Upload' : 'Lihat' ?> <?= $row->nama_kategori ?>
										</a>
										
										<?php if ($row->berkas != ''): ?>
											- <a href="<?= base_url('usulan/upload/'.$row->id_kat_berkas) ?>" data-toggle="tooltip" title="Ganti Berkas"><i class="fa fa-edit"></i></a>
										<?php endif; ?>
									</li>
								<?php
								}
							?>
						</ul>

						<br>
						<i>*) Berkas yang di-unggah disesuaikan dengan ketentuan fakultas masing-masing</i>
						
					</td>
					<td class="text-nowrap text-center">
						<?php if (count($aktivitas_mahasiswa) > 0): ?>
							<?php if ($aktivitas_mahasiswa[0]->nilai_angka != '' AND $aktivitas_mahasiswa[0]->nilai_mutu != ''): ?>
								<b><?= $aktivitas_mahasiswa[0]->nilai_angka ?> (<?= $aktivitas_mahasiswa[0]->nilai_mutu ?>)</b>
							<?php else: ?>
								<b data-toggle="tooltip" title="Nilai belum lengkap, silahkan hubungi bagian akademik fakultas." data-placement="left">-</b>
							<?php endif; ?>
						<?php else: ?>
							<b data-toggle="tooltip" title="Anda belum didaftarkan pada kegiatan <?= $usulan[0]->nm_mk ?>, Silahkan hubungi bagian akademik.">-</b>
						<?php endif; ?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>