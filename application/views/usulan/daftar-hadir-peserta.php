<html>
<head>
	<title><?= $title ?></title>
	<style type="text/css">
		@page {
			margin-top: 10mm;
			margin-header: 0;
			margin-footer: 5mm;
		}
	</style>
</head>
<body style="font-family: calibri; font-size: 11pt">
	<?php $this->load->view('usulan/kop-surat') ?>

	<table width="100%">
		<tr>
			<td>
				<center>
					<h3>
						DAFTAR HADIR MAHASISWA  <br>
						SEMINAR <?= strtoupper($usulan[0]->nm_mk) ?> (<?= acronym($usulan[0]->nm_mk) ?>) <br>
						<?= strtoupper($detail->nama_fak) ?> - UNIVERSITAS MAJALENGKA <br>
						TAHUN AKADEMIK <?= explode(' ', $usulan[0]->nama_semester)[0] ?>
					</h3>
				</center>
			</td>
		</tr>
	</table>
	<br>
	<table border="0" width="100%">
		<tr>
			<td>Hari, Tanggal</td>
			<td>:</td>
			<td><?= konversi_hari(date('w', strtotime($penjadwalan->tanggal))).', '.date_indo($penjadwalan->tanggal) ?></td>
		</tr>
		<tr>
			<td>Nama Mahasiswa</td>
			<td>:</td>
			<td><?= $detail->nm_pd ?></td>
		</tr>
		<tr>
			<td width="200">Nomor Pokok Mahasiswa</td>
			<td width="15">:</td>
			<td><?= $detail->id_mahasiswa_pt ?></td>
		</tr>
		<tr>
			<td>Program Studi</td>
			<td>:</td>
			<td><?= $detail->nama_prodi ?></td>
		</tr>
		<tr>
			<td valign="top">Judul</td>
			<td  valign="top">:</td>
			<td><?= count($aktivitas_mahasiswa) > 0 ? $aktivitas_mahasiswa[0]->judul : '-' ?></td>
		</tr>
	</table>
	<br>
	<table border="1" cellspacing="0" cellpadding="5" width="100%">
		<tr>
			<th width="15">NO</th>
			<th width="180">NAMA MAHASISWA</th>
			<th>NPM</th>
			<th>SMT</th>
			<th>PRODI</th>
			<th width="100">TANDA TANGAN</th>
		</tr>
		<?php for ($i=1; $i <= 15; $i++) { 
		?>
		<tr>
			<td align="center"><?= $i ?>.</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<?php
		} ?>
	</table>
	<br>
	<table width="100%">
		<tr>
			<td width="60%">
				&nbsp;
			</td>
			<td align="center">
				Majalengka, <?= date_indo(date('Y-m-d')) ?> <br>
				Ketua Sidang,
				<br>
				<br>
				<br>
				<br>
				<br>
				.......................................
			</td>
		</tr>
	</table>
</body>
</html>