<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Curl {

	function simple_get($url){
	    $ch = curl_init(); 
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    $output = curl_exec($ch);  
	    curl_close($ch);      
	    return $output;
	}

	function simple_post($url, $data){
	    $ch = curl_init(); 
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    $output = curl_exec($ch); 
	    curl_close($ch);      
	    return $output;
	}

	function simple_put($url, $data){
	    $ch = curl_init(); 
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	    $output = curl_exec($ch); 
	    curl_close($ch);      
	    return $output;
	}
}