<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//use \Firebase\JWT\JWT;

class Usulan extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		// if(empty($_SESSION['logged_in']) or $_SESSION['logged_in']==FALSE){ redirect ('https://satu.unma.ac.id');}
	}
	
	public function index()
	{
		if(empty($_SESSION['logged_in']) or $_SESSION['logged_in']==FALSE){ redirect ('https://satu.unma.ac.id');}
		$data['title']	='Usulan';
		$data['view']	='usulan/index';

		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		
		$data['aktivitas_mahasiswa'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/anggota?id_jenis_aktivitas_mahasiswa=6&id_mahasiswa_pt='.$_SESSION['id_user']));

		if (count($data['aktivitas_mahasiswa']) < 1) {
			redirect('auth/logout','refresh');
		}
		
		$data['pembimbing'] = [];
		$data['penguji'] = [];
		if (count($data['aktivitas_mahasiswa']) > 0) {
			$data['pembimbing'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/pembimbing?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas));
			$data['penguji'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/penguji?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas));
		}

		$data['penguji']	= json_decode($this->curl->simple_get(ADD_API.'aktivitas/penguji?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas)) ?: [];
		$data['penjadwalan']	= json_decode($this->curl->simple_get(ADD_API.'aktivitas/penjadwalan?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas)) ?: [];
		
		$data['kategori_berkas']	= json_decode($this->curl->simple_get(ADD_API.'ref/kategori_berkas?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas)) ?: [];

		$this->load->view('lyt/index', $data);
	}

	public function masukkan_judul()
	{
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$id_aktivitas = $this->input->post('id_aktivitas');
			$judul = urlencode(trim(preg_replace( "/\r|\n/", "", strip_tags($this->input->post('judul')))));

			$masukkan_judul = json_decode($this->curl->simple_get(ADD_API.'aktivitas/masukkan_judul?id_aktivitas='.$id_aktivitas.'&judul='.$judul));
			echo json_encode($masukkan_judul);
			exit;
		}

		$data['title']	='Masukkan Judul ';
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['view']	='usulan/masukkan-judul';
		$data['footer']	= [ 
							'<script src="'.CDN.'themes/vendors/js/editors/tinymce/tinymce.min.js"></script>',
							"<script>
								tinymce.init({
									selector: '.judul',
									height: 150,
									force_br_newlines : true,
  									force_p_newlines : true,
									toolbar_items_size : 'small',
									plugins: [ 'charmap', 'searchreplace' ],
									toolbar1: 'undo redo | copy paste | bold italic underline | subscript superscript | charmap',
									setup: function (editor) {
										editor.on('keydown', function (e) {
											if (e.keyCode === 13) {
												console.log('prevent default');
												e.preventDefault();
											}
										});
									},
									content_css: [
										'//www.tinymce.com/css/codepen.min.css'
									]
								});
							</script>"
						  ];

		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['aktivitas_mahasiswa'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/anggota?id_jenis_aktivitas_mahasiswa=6&id_mahasiswa_pt='.$_SESSION['id_user']));

		$this->load->view('lyt/index', $data);
	}

	public function atur_lokasi()
	{
		if (isset($_POST['lokasi'])) {
			$id_aktivitas = $this->input->post('id_aktivitas');
			// $lokasi = strip_tags(stripslashes($this->input->post('lokasi')));
			$lokasi = urlencode(trim(preg_replace( "/\r|\n/", "", strip_tags($this->input->post('lokasi')))));

			$atur_lokasi = json_decode($this->curl->simple_get(ADD_API.'aktivitas/atur_lokasi?id_aktivitas='.$id_aktivitas.'&lokasi='.$lokasi));
			echo json_encode($atur_lokasi);
			exit;
		}

		$data['title']	='Lokasi ';
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['view']	='usulan/atur-lokasi';
		$data['footer']	= [ 
							'<script src="'.CDN.'themes/vendors/js/editors/tinymce/tinymce.min.js"></script>',
							"<script>
								tinymce.init({
									selector: '.lokasi',
									height: 150,
									forced_root_block : '',
									force_br_newlines : true,
  									force_p_newlines : true,
  									force_p_newlines : false,
									toolbar_items_size : 'small',
									plugins: [ 'charmap', 'searchreplace' ],
									toolbar1: 'undo redo | copy paste | bold italic underline | subscript superscript | charmap',
									setup: function (editor) {
										editor.on('keydown', function (e) {
											if (e.keyCode === 13) {
												console.log('prevent default');
												e.preventDefault();
											}
										});
									},
									content_css: [
										'//www.tinymce.com/css/codepen.min.css'
									]
								});
							</script>"
						  ];

		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		$data['aktivitas_mahasiswa'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/anggota?id_jenis_aktivitas_mahasiswa=6&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));

		$this->load->view('lyt/index', $data);
	}

	public function upload($id_kat_berkas='')
	{	
		if ($id_kat_berkas == '') {
			redirect('usulan','refresh');
		}

		$title = json_decode($this->curl->simple_get(ADD_API.'aktivitas/berkas?id_kat_berkas='.$id_kat_berkas))[0];

		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		
		$data['title']  = $data['detail']->id_mahasiswa_pt.'_'.$data['detail']->nm_pd.'_'.$title->nama_kategori.' '.$data['usulan'][0]->nm_mk.'.pdf';

		// $data['title']  = $title->nama_kategori.' '.$data['usulan'][0]->nm_mk.' ('.$data['detail']->nm_pd.' - '.$data['detail']->id_mahasiswa_pt.').pdf';

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$config['upload_path']          = './berkas/kp/';
		    $config['allowed_types']        = 'pdf|ppt|pptx';
		    $config['file_name']            = $data['title'];
		    $config['overwrite']			= true;
		    $config['max_size']             = 10240; // 1MB

		    $this->load->library('upload', $config);

		    if ($this->upload->do_upload('berkas')) {
		        $data = [
		    		'id_kat_berkas' => $id_kat_berkas,
		    		'id_jenis_aktivitas_mahasiswa' => '6',
		    		'id_mahasiswa_pt' => $_SESSION['id_user'],
		    		'berkas' => $this->upload->data("file_name"),
		    		'upload_by' => $_SESSION['id_user']
		    	];

		        $this->curl->simple_post(ADD_API.'aktivitas/upload', $data);

		        redirect('usulan','refresh');
		    } else {
		    	echo $this->upload->display_errors();
		    }

 			// print_r($_FILES);
			exit;
		}

		$data['title']	='Upload Berkas '.$title->nama_kategori;
		$data['id_kat_berkas'] = $id_kat_berkas;
		$data['nama_kategori'] = $title->nama_kategori;

		$data['view']	='usulan/upload';
		$data['footer']	= [ 
							"<script>
								$('.custom-file input').change(function (e) {
							      $(this).next('.custom-file-label').html(e.target.files[0].name);
							      var src = URL.createObjectURL(event.target.files[0]);
							      var obj = document.querySelector('object');
							      obj.data = src
							  });
							</script>"
						  ];

		$this->load->view('lyt/index', $data);
	}

	public function pendaftaran($id_user='') {
		if ($id_user != '') {
			$_SESSION['id_user'] = $id_user;
		}

		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['title'] = 'Pendaftaran '.$data['usulan'][0]->nm_mk.' ('.$_SESSION['nama_user'].')';

		$mpdf = new \Mpdf\Mpdf([ 'mode'=>'utf-8', 'format'=>'FOLIO' ]);
		$html = $this->load->view('usulan/pendaftaran', $data, true);
		$mpdf->writeHTML(utf8_encode($html));
		$mpdf->SetHTMLFooter('
			<hr>
			<table width="100%" style="font-size:9pt;">
				<tr>
					<td width="50%" align="right"><i>Berkas ini dicetak oleh UNMAKU pada tanggal {DATE d/m/Y h:i:s}</i></td>
				</tr>
			</table>
		');

		$mpdf->output($data['title'].'.pdf', 'I');
	}

	public function pendaftaran_seminar($id_user='') {
		if ($id_user != '') {
			$_SESSION['id_user'] = $id_user;
		}

		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['title'] = 'Pendaftaran Seminar '.$data['usulan'][0]->nm_mk.' ('.$_SESSION['nama_user'].')';

		$data['aktivitas_mahasiswa'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/anggota?id_jenis_aktivitas_mahasiswa=6&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['pembimbing'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/pembimbing?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas));

		$mpdf = new \Mpdf\Mpdf([ 'mode'=>'utf-8', 'format'=>'FOLIO' ]);
		$html = $this->load->view('usulan/pendaftaran-seminar', $data, true);
		$mpdf->writeHTML(utf8_encode($html));
		$mpdf->SetHTMLFooter('
			<hr>
			<table width="100%" style="font-size:9pt;">
				<tr>
					<td width="50%" align="right"><i>Berkas ini dicetak oleh UNMAKU pada tanggal {DATE d/m/Y h:i:s}</i></td>
				</tr>
			</table>
		');

		$mpdf->output($data['title'].'.pdf', 'I');
	}

	public function berita_acara($id_user='') {
		if ($id_user != '') {
			$_SESSION['id_user'] = $id_user;
			$_SESSION['nama_smt'] = $_REQUEST['nama_smt'];
			$data['data'] = $_REQUEST['data'];

			if ($data['data'] == 1) {
				$anggota = json_decode($this->curl->simple_get(ADD_API.'aktivitas/anggota?id_jenis_aktivitas_mahasiswa=6&id_mahasiswa_pt='.$_SESSION['id_user']));

				$data['nilai_pembimbing'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/get_nilai?id_kegiatan=1&jenis_nilai=1&id_anggota='.$anggota['0']->id_anggota.'&id_aktivitas='.$anggota[0]->id_aktivitas));
				$data['nilai_penguji'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/get_nilai?id_kegiatan=1&jenis_nilai=2&id_anggota='.$anggota['0']->id_anggota.'&id_aktivitas='.$anggota[0]->id_aktivitas));
			}
		}

		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['aktivitas_mahasiswa'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/anggota?id_jenis_aktivitas_mahasiswa=6&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['pembimbing'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/pembimbing?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas));
		$data['penguji']	= json_decode($this->curl->simple_get(ADD_API.'aktivitas/penguji?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas)) ?: [];
		$data['penjadwalan']	= json_decode($this->curl->simple_get(ADD_API.'aktivitas/penjadwalan?id_kegiatan=1&id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas))[0];

		$data['title'] = 'Berita Acara '.$data['usulan'][0]->nm_mk.' ('.$_SESSION['nama_user'].')';
		$mpdf = new \Mpdf\Mpdf([ 'mode'=>'utf-8', 'format'=>'FOLIO' ]);
		
		$berita_acara = $this->load->view('usulan/berita-acara', $data, true);
		$mpdf->writeHTML(utf8_encode($berita_acara));
		
		$mpdf->addPage();
		$form_penilaian = $this->load->view('usulan/form-penilaian', $data, true);
		$mpdf->writeHTML(utf8_encode($form_penilaian));

		$mpdf->addPage();
		$catata_revisi_pembimbing = $this->load->view('usulan/catatan-revisi-pembimbing', $data, true);
		$mpdf->writeHTML(utf8_encode($catata_revisi_pembimbing));

		if (count($data['penguji']) > 0) {
			$mpdf->addPage();
			$catata_revisi_penguji = $this->load->view('usulan/catatan-revisi-penguji', $data, true);
			$mpdf->writeHTML(utf8_encode($catata_revisi_penguji));
		}
		
		$mpdf->SetHTMLFooter('
			<hr>
			<table width="100%" style="font-size:9pt;">
				<tr>
					<td width="50%" align="right"><i>Berkas ini dicetak oleh UNMAKU pada tanggal {DATE d/m/Y h:i:s}</i></td>
				</tr>
			</table>
		');

		$mpdf->output($data['title'].'.pdf', 'I');
	}

	public function berita_acara_lengkap($id_user='') {
		if ($id_user != '') {
			$_SESSION['id_user'] = $id_user;
			$_SESSION['nama_smt'] = $_REQUEST['nama_smt'];
		}

		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['aktivitas_mahasiswa'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/anggota?id_jenis_aktivitas_mahasiswa=6&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['pembimbing'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/pembimbing?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas));
		$data['penguji']	= json_decode($this->curl->simple_get(ADD_API.'aktivitas/penguji?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas)) ?: [];
		$data['penjadwalan']	= json_decode($this->curl->simple_get(ADD_API.'aktivitas/penjadwalan?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas))[0];

		$data['title'] = 'Berita Acara '.$data['usulan'][0]->nm_mk.' ('.$_SESSION['nama_user'].')';
		$mpdf = new \Mpdf\Mpdf([ 'mode'=>'utf-8', 'format'=>'FOLIO' ]);
		
		$berita_acara = $this->load->view('usulan/berita-acara', $data, true);
		$mpdf->writeHTML(utf8_encode($berita_acara));
		
		$mpdf->addPage();
		$form_penilaian = $this->load->view('usulan/form-penilaian', $data, true);
		$mpdf->writeHTML(utf8_encode($form_penilaian));

		$mpdf->addPage();
		$catata_revisi_pembimbing = $this->load->view('usulan/catatan-revisi-pembimbing', $data, true);
		$mpdf->writeHTML(utf8_encode($catata_revisi_pembimbing));

		if (count($data['penguji']) > 0) {
			$mpdf->addPage();
			$catata_revisi_penguji = $this->load->view('usulan/catatan-revisi-penguji', $data, true);
			$mpdf->writeHTML(utf8_encode($catata_revisi_penguji));
		}
		
		$mpdf->SetHTMLFooter('
			<hr>
			<table width="100%" style="font-size:9pt;">
				<tr>
					<td width="50%" align="right"><i>Berkas ini dicetak oleh UNMAKU pada tanggal {DATE d/m/Y h:i:s}</i></td>
				</tr>
			</table>
		');

		$mpdf->output($data['title'].'.pdf', 'I');
	}

	public function daftar_hadir_peserta($id_user='') {
		if ($id_user != '') {
			$_SESSION['id_user'] = $id_user;
			$_SESSION['nama_smt'] = $_REQUEST['nama_smt'];
		}

		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['aktivitas_mahasiswa'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/anggota?id_jenis_aktivitas_mahasiswa=6&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['pembimbing'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/pembimbing?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas));
		$data['penguji']	= json_decode($this->curl->simple_get(ADD_API.'aktivitas/penguji?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas)) ?: [];
		$data['penjadwalan']	= json_decode($this->curl->simple_get(ADD_API.'aktivitas/penjadwalan?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas))[0];

		$data['title'] = 'Daftar Hadir Peserta Seminar '.$data['usulan'][0]->nm_mk.' ('.$_SESSION['nama_user'].')';
		$mpdf = new \Mpdf\Mpdf([ 'mode'=>'utf-8', 'format'=>'FOLIO' ]);
		
		$berita_acara = $this->load->view('usulan/daftar-hadir-peserta', $data, true);
		$mpdf->writeHTML(utf8_encode($berita_acara));
		
		$mpdf->SetHTMLFooter('
			<hr>
			<table width="100%" style="font-size:9pt;">
				<tr>
					<td width="50%" align="right"><i>Berkas ini dicetak oleh UNMAKU pada tanggal {DATE d/m/Y h:i:s}</i></td>
				</tr>
			</table>
		');

		$mpdf->output($data['title'].'.pdf', 'I');
	}

	public function laporan_kemajuan($id_user='') {
		if ($id_user != '') {
			$_SESSION['id_user'] = $id_user;
		}

		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['title'] = ($data['usulan'][0]->nm_mk == 'Kerja Praktek' ? 'Laporan Kemajuan ' : 'Jurnal ').$data['usulan'][0]->nm_mk.' ('.$_SESSION['nama_user'].')';
		$data['aktivitas_mahasiswa'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/anggota?id_jenis_aktivitas_mahasiswa=6&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['pembimbing'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/pembimbing?id_aktivitas='.$data['aktivitas_mahasiswa'][0]->id_aktivitas));

		$mpdf = new \Mpdf\Mpdf([ 'mode'=>'utf-8', 'format'=>'FOLIO' ]);
		$html = $this->load->view('usulan/laporan-kemajuan', $data, true);
		$mpdf->writeHTML(utf8_encode($html));
		$mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:9pt;">
				<tr>
					<td width="50%" align="right"><i>Berkas ini dicetak oleh UNMAKU pada tanggal {DATE d/m/Y h:i:s}</i></td>
				</tr>
			</table>
		');

		$mpdf->output($data['title'].'.pdf', 'I');
	}

	public function bukti_penyerahan_laporan($id_user='') {
		if ($id_user != '') {
			$_SESSION['id_user'] = $id_user;
		}

		$data['detail'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/mahasiswa_pt?id_mahasiswa_pt='.$_SESSION['id_user']))[0];
		$data['usulan'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/usulan?id_kat_mk=3&id_mahasiswa_pt='.$_SESSION['id_user']));
		$data['title'] = 'Bukti Penyerahan Laporan '.$data['usulan'][0]->nm_mk.' ('.$_SESSION['nama_user'].')';
		$data['aktivitas_mahasiswa'] = json_decode($this->curl->simple_get(ADD_API.'aktivitas/anggota?id_jenis_aktivitas_mahasiswa=6&id_mahasiswa_pt='.$_SESSION['id_user']));

		$mpdf = new \Mpdf\Mpdf([ 'mode'=>'utf-8', 'format'=>'FOLIO' ]);
		$html = $this->load->view('usulan/bukti-penyerahan-laporan', $data, true);
		$mpdf->writeHTML(utf8_encode($html));
		$mpdf->SetHTMLFooter('
			<table width="100%" style="font-size:9pt;">
				<tr>
					<td width="50%" align="right"><i>Berkas ini dicetak oleh UNMAKU pada tanggal {DATE d/m/Y h:i:s}</i></td>
				</tr>
			</table>
		');
		$mpdf->output($data['title'].'.pdf', 'I');
	}
}